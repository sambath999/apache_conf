### Apache_Conf [![Latest Stable Version](https://poser.pugx.org/apache_nginx_conf/apache_conf/v)](//packagist.org/packages/apache_nginx_conf/apache_conf) [![Total Downloads](https://poser.pugx.org/apache_nginx_conf/apache_conf/downloads)](//packagist.org/packages/apache_nginx_conf/apache_conf) [![Latest Unstable Version](https://poser.pugx.org/apache_nginx_conf/apache_conf/v/unstable)](//packagist.org/packages/apache_nginx_conf/apache_conf) [![License](https://poser.pugx.org/apache_nginx_conf/apache_conf/license)](//packagist.org/packages/apache_nginx_conf/apache_conf)

This is a set of classes for working with configuration files of the **Apache2** web server.

Code licensed under **Apache License Version 2.0**.

### Requirements

* PHP >= 7.0.0, <= 8.1.6;
* Apache2 >= 2.4;

**NOTE:** This package was originally written by [[Aleksey_Nemiro](https://packagist.org/packages/aleksey.nemiro/apacheconf.php)]
I have just cloned and fixed composer autoload problem and php8 compatibility issues then published without touching original project.
I do not own any of this project and/or properties. 

### How to use?

Include **autoload.php** file and import the class ``Apache\Conf``.

```PHP
# include autoload.php from composer vendor
require_once 'vendor/autoload.php';

# import class
use Apache\Conf as ApacheConf;
```

#### Load config from file

```PHP
# create instance and load config from file
$conf = new ApacheConf('/etc/apache2/sites-available/example.org.conf');
# or
# $conf = ApacheConf::CreateFromFile('/etc/apache2/sites-available/example.org.conf');

# get values
var_dump($conf['VirtualHost']);
var_dump($conf['VirtualHost']->ParametersAsString());
var_dump($conf['VirtualHost']['DocumentRoot']->ParametersAsString());
var_dump($conf['VirtualHost']['ServerName']->ParametersAsString());
var_dump($conf['VirtualHost']['Alias']);
```

#### Load config from string

```PHP
# config data
$str = '<VirtualHost 127.0.0.1:80>
	DocumentRoot /home/example.org/www
  ServerName example.org

  <Location />
	  AuthType Basic
		AuthUserFile users.pwd
		Require valid-user
	</Location>
</VirtualHost>';

# parse string
$conf = ApacheConf::CreateFromString($str);

# get values
var_dump($conf['VirtualHost']);
var_dump($conf['VirtualHost']->ParametersAsString());
var_dump($conf['VirtualHost']['ServerName']->ParametersAsString());

# get location
$location = $conf['VirtualHost']['Location'][0];
var_dump($location);
```

#### Save to file

```PHP
# load from file
$conf = ApacheConf::CreateFromFile('/etc/apache2/sites-available/example.org.conf');

# set values
$conf['VirtualHost']['ServerName']->Parameters = array('example.org', 'www.example.org');
$conf['VirtualHost']['DocumentRoot']->Parameters = array('/home/example.org/www');

# create a new directive
$new_directory = ApacheConf::CreateDirective('Directory');
$new_directory->AddDirective('AllowOverride', 'All');
$new_directory->AddDirective('Allow', array('from', 'all'));
$new_directory->AddDirective('Require', array('all', 'granted'));

# add the new Directory section to the VirtualHost section
$new_directory->AddTo($conf['VirtualHost']);

# save
$conf->Save();

# or save as...
# $conf->Save('newFileName.conf');
```

#### Get string from current instance

```PHP
# load from file
$conf = new ApacheConf::CreateFromFile('/etc/apache2/sites-available/example.org.conf');

# set values
$conf['VirtualHost']['ServerName']->Parameters = array('example.org', 'www.example.org');
$conf['VirtualHost']['DocumentRoot']->Parameters = array('/home/example.org/www');

# create a new directive
$new_directory = ApacheConf::CreateDirective('Directory');
$new_directory->AddDirective('AllowOverride', 'All');
$new_directory->AddDirective('Allow', array('from', 'all'));
$new_directory->AddDirective('Require', array('all', 'granted'));

# add the new Directory section to the VirtualHost section
$new_directory->AddTo($conf['VirtualHost']);

# get as string
$string = $conf->GetAsString();

# show string
var_dump($string);
```

#### Create a new config

```PHP
# create an instance
$conf = new ApacheConf();

# create VirtualHost
$virtualHost = ApacheConf::CreateDirective('VirtualHost', '192.168.100.39:8080');
$virtualHost->AddDirective('DocumentRoot', '/home/example.org/www');
$virtualHost->AddDirective('ServerName', 'example.org');

# add to conf
$conf->Add($virtualHost);

# create directory
$directory = ApacheConf::CreateDirective('Directory');
$directory->AddDirective('AllowOverride', 'All');
$directory->AddDirective('Allow', array('from', 'all'));
$directory->AddDirective('Require', array('all', 'granted'));

# add the new Directory section to the VirtualHost section
$directory->AddTo($virtualHost);

# get as string
$string = $conf->GetAsString();

# show string
var_dump($string);

# or save
# $conf->Save('newFileName.conf');
```